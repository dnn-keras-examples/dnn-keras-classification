## Deep Neural Network for classification in Python Keras 2+

Quick-start tutorial for those starting with Keras.
Ipython notebook is self-explanatory.

### Displaying in browser

Just open the ```keras-classification.ipynb``` file

### Running on Ubuntu

1. git clone https://gitlab.com/dnn-keras-examples/dnn-keras-classification
1. cd dnn-keras-classification
1. sudo apt-get install python-virtualenv
1. virtualenv --python=python3 .venv
1. source .venv/bin/activate
1. pip install -r requirements.txt
1. jupyter notebook


Will also run on any other operating system. Adjust the commands according to your distro.
